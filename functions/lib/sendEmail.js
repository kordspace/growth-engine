const sgMail = require('@sendgrid/mail');

// Welcome Email.
// Cludged together lovingly from https://github.com/firebase/functions-samples/blob/master/quickstarts/email-users/functions/index.js
// and https://www.npmjs.com/package/@sendgrid/mail

sgMail.setApiKey('Enter API key');

// exports.sendWelcomeEmail = functions.auth.user().onCreate((user) => {
//     // [END onCreateTrigger]
//     // [START eventAttributes]
//     const email = user.email; // The email of the user.
//     // [END eventAttributes]

//     return sendWelcomeEmail(email);
// });

// Sends a welcome email to the given user.
const sendWelcomeEmail = async (recipient) => {
  let html =
    '<p>Welcome to Khalil for Congress Volunteer App! We are excited to work with you! </p>';
  html +=
    '<p>We will be using the REACH app for our canvassing efforts. You can find a link to download it here: <a href="https://www.thereachapp.com/">https://www.thereachapp.com/</a></p>';
  html +=
    '<p>We are using Discord for most volunteer and staff communications. Sign up for our channel here: <a href="https://discord.gg/J7DZfVu">https://discord.gg/J7DZfVu</a></p>';
  html +=
    '<p>Congratulations! You are officially part of the campaign! Together we will flip Congressional District Three and will make life better for hundreds of thousands of Washingtonians and folks all across the country!</p>';
  html +=
    '<p>If you have any questions feel free to post them on the Discord or email the administrator at <a href="mailto:eshelton@khalilforcongress.com">eshelton@khalilforcongress.com</a>  if you have any questions!</p>';

  sgMail.send({
    from: 'noreply@peter-for-congress.firebaseapp.com',
    to: recipient,
    subject: 'Welcome to Khalil for Congress Volunteer App!',
    html,
  });
};
