const fs = require('fs');
const os = require('os');
const path = require('path');
const admin = require('firebase-admin');
const im = require('imagemagick');

const identifyFIle = (file) =>
  new Promise((resolve, reject) => {
    im.identify(file, (err, features) => {
      if (err) reject(e);
      resolve([features.width, features.height]);
    });
  });

const squareFIle = (file, size) =>
  new Promise((resolve, reject) => {
    im.crop(
      {
        srcPath: file,
        dstPath: file,
        width: size,
        height: size,
        quality: 1,
        gravity: 'North',
      },
      (err) => {
        if (err) reject(err);
        else resolve('success');
      }
    );
  });

module.exports = async ({ name, type, storageFolder }) => {
  try {
    const fileBucket = 'growth-engine-295114.appspot.com';

    console.log('---------------------------------------------------------------');
    console.log(`FILE PATH: ${name}`);
    console.log(`FILE BUCKET: ${fileBucket}`);
    console.log(`FILE TYPE: ${type}`);
    console.log('---------------------------------------------------------------');

    // Exit if this is triggered on a file that is not an image
    if (!type.startsWith('image/')) {
      return console.log('This is not an image.');
    }

    const bucket = admin.storage().bucket(fileBucket);
    const tempFilePath = path.join(os.tmpdir(), name);
    const metadata = {
      contentType: type,
    };

    console.log(`FILE NAME: ${name}`);
    console.log(`TEMP FILE PATH: ${tempFilePath}`);
    console.log('---------------------------------------------------------------');

  ////////////////////////////////////////
    // const [width, height] = await identifyFIle(`${storageFolder}/${name}`);

    // console.log(`FILE SIZE: ${width} and ${height}`);
    // console.log('---------------------------------------------------------------');
    // return false;

    // Download file from bucket.
    await bucket.file(`${storageFolder}/${name}`).download({ destination: tempFilePath });

    console.log('FILE TO SQUARE DOWNLOADED FROM', `${storageFolder}/${name}`);
    console.log('---------------------------------------------------------------');

    // The size of the square
    const [width, height] = await identifyFIle(tempFilePath);
    const size = width < height ? width : height;
    const diff = Math.abs(width - height);

    // Exit if the image does not need to be squared (ratio height/width acceptable)
    if ((100 / size) * diff < 10) return;

    // Make a squared image if the width ad height are over 10% different
      console.log('SQUARING IMAGE WITH IMAGE MAGICK');
      console.log('---------------------------------------------------------------');
      // Generate a squared image using ImageMagick
      await squareFIle(tempFilePath, size);
      console.log('SQUARED IMAGE CREATED AT', tempFilePath);
      console.log('---------------------------------------------------------------');

    // Uploading the file
    const squaredFilePath = path.join(storageFolder, name);
    const data = await bucket.upload(tempFilePath, {
      destination: squaredFilePath,
      metadata: metadata,
    });

    console.log(`SQUARE FILE PATH: ${squaredFilePath}`);
    console.log('---------------------------------------------------------------');

    // Delete the local files to free up disk space.
    fs.unlinkSync(tempFilePath);

    return true;
  } catch (error) {
    console.log('>>>>> Error', error.toString());
    // Delete the local files to free up disk space.
    fs.unlinkSync(path.join(os.tmpdir(), name));
    return false;
  }
};
