const fs = require('fs');
const os = require('os');
const path = require('path');
const admin = require('firebase-admin');
const im = require('imagemagick');

const identifyFIle = (file) =>
  new Promise((resolve, reject) => {
    im.identify(file, (err, features) => {
      if (err) reject(e);
      resolve([features.width, features.height]);
    });
  });

const squareFIle = (file, size) =>
  new Promise((resolve, reject) => {
    im.crop(
      {
        srcPath: file,
        dstPath: file,
        width: size,
        height: size,
        quality: 1,
        gravity: 'North',
      },
      (err, stdout, stderr) => {
        if (err) reject(err);
        resolve('success');
        console.log('stdout:', stdout);
      }
    );
  });

module.exports = async (object) => {
  try {
    const filePath = object.name; // File path in the bucket.
    const fileBucket = object.bucket; // The Storage bucket that contains the file.
    const contentType = object.contentType; // File content type.
    const destinationFolder = 'avatars';

    console.log('---------------------------------------------------------------');
    console.log(`FILE PATH: ${filePath}`);
    console.log(`FILE BUCKET: ${fileBucket}`);
    console.log(`CONTENT TYPE: ${contentType}`);
    console.log('---------------------------------------------------------------');

    // Exit if this is triggered on a file that is not a profile picture.
    if (path.dirname(filePath) !== 'profilePics') {
      return console.log('This is not a profile picture');
    }

    // Exit if this is triggered on a file that is not an image
    if (!contentType.startsWith('image/')) {
      return console.log('This is not an image.');
    }

    // Get the file name
    const fileName = path.basename(filePath);

    // Download file from bucket.
    const bucket = admin.storage().bucket(fileBucket);
    const tempFilePath = path.join(os.tmpdir(), fileName);
    const metadata = {
      contentType: contentType,
    };

    console.log('---------------------------------------------------------------');
    console.log(`FILE NAME: ${fileName}`);
    console.log(`TEMP FILE PATH: ${tempFilePath}`);
    console.log('---------------------------------------------------------------');

    await bucket.file(filePath).download({ destination: tempFilePath });

    // The size of the square
    const [width, height] = await identifyFIle(tempFilePath);
    const size = width < height ? width : height;
    const diff = Math.abs(width - height);

    // Make a squared image if the width ad height are over 10% different
    if ((100 / size) * diff > 10) {
      // Generate a squared image using ImageMagick
      await squareFIle(tempFilePath, size);
      console.log('Squared image created at', tempFilePath);
    }

    // Uploading the file
    const squaredFilePath = path.join(destinationFolder, fileName);
    const data = await bucket.upload(tempFilePath, {
      destination: squaredFilePath,
      metadata: metadata,
    });

    console.log('---------------------------------------------------------------');
    console.log(`SQUARE FILE PATH: ${squaredFilePath}`);
    console.log('UPLOAD RESULT DATA');
    console.log(data);
    console.log('---------------------------------------------------------------');

    // Delete the file first uploaded that trigger this function
    // await bucket.file(filePath).delete();
    // Delete the local files to free up disk space.
    fs.unlinkSync(tempFilePath);

    return true;
  } catch (error) {
    console.log('Error', error.toString());
    // Delete the local files to free up disk space.
    fs.unlinkSync(path.join(os.tmpdir(), path.basename(object.name)));
    return false;
  }
};
