const axios = require('axios');
const credentials = require('../.credentials');

const auth = credentials.gtpServerAuth;

module.exports = async (params) => {
  try {
    const { parallelComputation, ...gptParams } = params;
    const fn = parallelComputation ? 'generateInParallel' : 'generate';
    const gpt2Endpoint = `https://aos-gpt-server.ngrok.io/${fn}`;
    const response = await axios.post(gpt2Endpoint, gptParams, { auth });
    return response.data;
  } catch (error) {
    console.log('>>>> Error:', error.toString());
    return { error: error.toString() };
  }
};
