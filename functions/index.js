const admin = require('firebase-admin');
const functions = require('firebase-functions');
const squareImage = require('./lib/squareImage');
const generateText = require('./lib/generateText');
const config = require('./config');

admin.initializeApp({
  storageBucket: config.bucketName,
});

// const algoliasearch = require('algoliasearch');
// const { app_id, api_key } = functions.config().algolia;
// const client = algoliasearch(app_id, api_key);
// Pushing user data into Algolia index when new user is created
// exports.onUserCreated = functions.firestore
//   .document('TEST-users/{userId}')
//   .onCreate((snap, context) => {
//     const userData = snap.data();
//     // Add an 'objectID' field which Algolia requires
//     userData.objectID = context.params.userId;
//     // Write to the algolia index
//     const index = client.initIndex('users');
//     return index.saveObject(userData);
//   });

// exports.squareImage = functions.storage.object().onFinalize(squareImage);

exports.squareImage = functions.https.onCall(squareImage);

exports.generateText = functions.https.onCall(generateText);
