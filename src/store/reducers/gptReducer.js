export default (state = null, { type, data }) => {
  if (type === 'SET_GPT_MODELS') {
    return data;
  }
  return state;
};
