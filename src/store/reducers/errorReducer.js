export default (state = null, { type, data }) => {
  if (type === 'DISPLAY_ERROR') {
    return data;
  }
  if (type === 'CLEAR_ERROR') {
    return null;
  }
  return state;
};
