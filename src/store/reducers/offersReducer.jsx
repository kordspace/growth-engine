// Orders Data
const offers = {
  columns: [
    {
      Header: "TITLE",
      accessor: "title"
    },
    {
      Header: "DESCRIPTION",
      accessor: "description"
    },
    {
      Header: "IMAGE",
      accessor: "image"
    },
    {
      Header: "LINK",
      accessor: "link"
    },
    {
      Header: "CATEGORY",
      accessor: "category"
    },
    {
      Header: "LINK",
      accessor: "link"
    }
  ],
  dataLoaded: false
};

export const initState = {
  offers
};

export default (state = initState, action) => {
  switch (action.type) {
    case "SET_OFFERS_LIST":
      console.log("SET_OFFERs_LIST");
      return {
        ...state,
        offers: {
          ...state.offers,
          dataRows: action.dataRows,
          dataLoaded: true
        }
      };
    default:
      return state;
  }
};
