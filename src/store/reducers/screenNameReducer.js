export default (state = null, action) => {
  if (action.type == 'SET_SCREEN_NAME') {
    return action.data;
  }
  return state;
};
