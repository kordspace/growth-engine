import React from "react";

import {
  getFirestoreData,
  updateFirestoreWithOneObject
} from "store/actions/firestoreActions.jsx";
import { startSetUser, updateUser } from "store/actions/profileActions.jsx";
import moment from "moment";
import { db, firebase } from "firebase/fbConfig.js";
import Button from "components/CustomButtons/Button.jsx";

// Get Users for Users Panel
export const startSetClientThreadsList = (uid, id) => {
  return dispatch => {
    db.collection("TEST-users")
      .doc(uid)
      .collection("cases")
      .doc(id)
      .collection("threads")
      .get()
      .then(
        snapshot => {
          const data = [];
          // Parse data into array like in firebase.js
          snapshot.forEach(doc => {
            var docData = doc.data();

            var docObj = {
              id: doc.id,
              threadName: docData.threadName ? docData.threadName : "N/A",
              createdDate: docData.createdDate ? docData.createdDate : "N/A"
            };
            data.push(docObj);
          });
          return data;
        },
        error => {
          console.log("error fetching data: ", error);
        }
      )
      .then(data => {
        dispatch(setClientThreadsList(data));
      });
  };
};

// Get Users for Users Panel
export const startGetCurrentClientThread = (uid, caseID, threadID) => {
  return dispatch => {
    db.collection("TEST-users")
      .doc(uid)
      .collection("cases")
      .doc(caseID)
      .collection("threads")
      .doc(threadID)
      .get()
      .then(
        snapshot => {
          var docData = snapshot.data();
          if (docData) {
            var docObj = {
              id: snapshot.id,
              threadName: docData.threadName ? docData.threadName : "N/A"
            };
            return docObj;
          } else {
            return null;
          }
        },
        error => {
          console.log("error fetching data: ", error);
        }
      )
      .then(data => {
        dispatch(getCurrentClientThread(data));
      });
  };
};

export const setClientThreadsList = dataRows => ({
  type: "SET_CLIENT_THREADS_LIST",
  dataRows
});

export const getCurrentClientThread = dataRows => ({
  type: "GET_CURRENT_CLIENT_THREAD",
  dataRows
});
