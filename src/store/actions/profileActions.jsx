export const updateUser = (data) => {
  return {
    type: 'UPDATE_USER_DATA',
    data,
  };
};

export const logout = () => ({
  type: 'LOG_OUT',
});

// _TODO: remove
export const startSetUser = () => null;
