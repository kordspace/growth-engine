export const setScreenName = (name) => ({
  type: 'SET_SCREEN_NAME',
  data: name,
});
