export const displayError = (data) => ({
  type: 'DISPLAY_ERROR',
  data,
});

export const clearError = () => ({
  type: 'CLEAR_ERROR',
});
