// ##############################
// // // Footer styles
// #############################

import {
  defaultFont,
  container,
  containerFluid,
  primaryColor,
  darkColor,
  boxShadow
} from "assets/jss/material-dashboard-pro-react.jsx";
import { Z_FIXED } from "zlib";

const footerStyle = theme => ({
  block: {},
  left: {
    float: "left!important",
    display: "block"
  },
  right: {
    margin: "0",
    fontSize: "14px",
    float: "right!important",
    padding: "15px"
  },
  footer: {
    background: '#7E7E7E',
    bottom: "0",
    ...boxShadow,
    padding: "2px 0",
    ...defaultFont,
    zIndex: "1029",
    position: "fixed",
    display: "none",
    [theme.breakpoints.up("md")]: {
      display: "none"
    }
  },
  container: {
    zIndex: 3,
    ...container,
    position: "relative"
  },
  containerFluid: {
    zIndex: 3,
    ...containerFluid,
    position: "relative",
  },
  a: {
    color: primaryColor,
    textDecoration: "none",
    backgroundColor: "transparent"
  },
  list: {
    marginBottom: "0",
    padding: "0",
    marginTop: "0"
  },
  inlineBlock: {
    display: "inline-block",
    padding: "0",
    width: "auto"
  },
  whiteColor: {
    "&,&:hover,&:focus": {
      color: "#FFFFFF"
    }
  }
});
export default footerStyle;
