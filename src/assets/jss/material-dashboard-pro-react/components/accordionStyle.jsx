import { primaryColor } from "assets/jss/material-dashboard-pro-react.jsx";

const accordionStyle = theme => ({
  root: {
    flexGrow: 1,
    marginLeft: "-50px",
    paddingLeft: "-5px"
  },
  expansionPanel: {
    boxShadow: "none",
    backgroundColor: "#1B3D59",
    "&:before": {
      display: "none !important"
    }
  },
  expansionPanelExpanded: {
    margin: "0"
  },
  expansionPanelSummary: {
    minHeight: "auto !important",
    backgroundColor: "transparent",
    padding: "25px 0px 5px 0px",
    borderTopLeftRadius: "3px",
    borderTopRightRadius: "3px",
    color: "#FFFFFF",
    "&:hover": {
      color: primaryColor
    }
  },
  expansionPanelSummaryExpaned: {
    color: primaryColor,
    "& $expansionPanelSummaryExpandIcon": {
      [theme.breakpoints.up("md")]: {
        top: "auto !important"
      },
      transform: "rotate(180deg)",
      [theme.breakpoints.down("sm")]: {
        top: "10px !important"
      }
    }
  },
  expansionPanelSummaryContent: {
    margin: "0 !important"
  },
  expansionPanelSummaryExpandIcon: {
    [theme.breakpoints.up("md")]: {
      top: "auto !important"
    },
    transform: "rotate(0deg)",
    color: "white",
    [theme.breakpoints.down("sm")]: {
      top: "5px !important"
    }
  },
  expansionPanelSummaryExpandIconExpanded: {},
  title: {
    fontSize: "15px",
    fontWeight: "bolder",
    marginTop: "-5px",
    marginBottom: "10px",
    color: "#FFFFFF",
    padding: "0px 10px"
  },
  expansionPanelDetails: {
    padding: "15px 10px",
    color: "#FFFFFF"
  }
});

export default accordionStyle;
