// ##############################
// // // Dashboard View styles
// #############################

import {
  bigNumbers,
  primaryColor,
  dangerColor,
  successColor,
  tooltip,
  bodyText,
  card,
  cardTitle,
  cardSubtitle,
  title
} from "assets/jss/material-dashboard-pro-react.jsx";

import modalStyle from "assets/jss/material-dashboard-pro-react/modalStyle.jsx";
import cardImagesStyles from "assets/jss/material-dashboard-pro-react/cardImagesStyles.jsx";
import hoverCardStyle from "assets/jss/material-dashboard-pro-react/hoverCardStyle.jsx";

const dashboardStyle = theme => ({
  ...modalStyle(theme),
  ...cardImagesStyles,
  // Profile Pic
  profilePic: {
    borderRadius: "0px",
    display: "block",
    width: "100%",
    marginBottom: "5px"
  },
  primary: {
    backgroundColor: primaryColor,
    color: "#FFFFFF"
  },
  dangerIcon: {
    color: dangerColor
  },
  successIcon: {
    color: successColor
  },
  bigNumbers: {
    ...bigNumbers
  },
  ...card,
  ...hoverCardStyle,
  tooltip,
  cardTitle: {
    ...cardTitle,
    marginTop: "0px",
    marginBottom: "3px"
  },
  title,
  modalTitle: {
    ...title,
    color: "#fff",
    marginBottom: "0",
    marginTop: "0"
  },
  subTitle: {
    ...title,
    color: primaryColor,
    marginTop: "0",
    marginBottom: "0"
  },
  [theme.breakpoints.down("sm")]: {
    eternal: {
      position: "fixed",
      bottom: "75px",
      width: "60%",
      border: "transparent",
      color: "#1C3E59",
      padding: "12px 0px 15px 0px",
      zIndex: "1200",
      background:
        "linear-gradient(90deg, rgba(143,206,159,1) 0%, rgba(241,232,102,1) 100%)",
      borderRadius: "50px",
      boxShadow: "0px 0px 15px -5px rgba(0,0,0,0.75)",
      fontWeight: "600",
      fontSize: "16px",
      animation: "MoveUpDown 2s linear infinite"
    }
  },
  [theme.breakpoints.up("md")]: {
    eternal: {
      position: "fixed",
      bottom: "20px",
      width: "30%",
      border: "transparent",
      color: "#1C3E59",
      padding: "20px 0px 20px 0px",
      zIndex: "99",
      background:
        "linear-gradient(90deg, rgba(143,206,159,1) 0%, rgba(241,232,102,1) 100%)",
      borderRadius: "50px",
      boxShadow: "0px 0px 15px -5px rgba(0,0,0,0.75)",
      fontWeight: "600",
      fontSize: "16px",
      animation: "MoveUpDown 2s linear infinite"
    }
  },
  body: {
    ...bodyText
  },
  cardSubtitle: {
    ...title,
    marginTop: "0",
    marginBottom: "0"
  },
  cardIconTitle: {
    ...cardTitle,
    marginTop: "15px",
    marginBottom: "0px"
  },
  cardProductTitle: {
    ...cardTitle,
    marginTop: "0px",
    marginBottom: "3px",
    textAlign: "center"
  },
  cardCategory: {
    color: "#999999",
    fontSize: "14px",
    paddingTop: "10px",
    marginBottom: "0",
    marginTop: "0",
    margin: "0"
  },
  cardProductDesciprion: {
    textAlign: "center",
    color: "#999999"
  },
  stats: {
    color: "#999999",
    fontSize: "12px",
    lineHeight: "22px",
    display: "inline-flex",
    "& svg": {
      position: "relative",
      top: "4px",
      width: "16px",
      height: "16px",
      marginRight: "3px"
    },
    "& .fab,& .fas,& .far,& .fal,& .material-icons": {
      position: "relative",
      top: "4px",
      fontSize: "16px",
      marginRight: "3px"
    }
  },
  productStats: {
    paddingTop: "7px",
    paddingBottom: "7px",
    margin: "0"
  },
  successText: {
    color: successColor
  },
  upArrowCardCategory: {
    width: 14,
    height: 14
  },
  underChartIcons: {
    width: "17px",
    height: "17px"
  },
  price: {
    color: "inherit",
    "& h4": {
      marginBottom: "0px",
      marginTop: "0px"
    }
  }
});

export default dashboardStyle;
