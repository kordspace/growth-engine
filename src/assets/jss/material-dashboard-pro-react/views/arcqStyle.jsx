// ##############################
// // // Dashboard View styles
// #############################

import {
  bigNumbers,
  primaryColor,
  dangerColor,
  successColor,
  tooltip,
  cardTitle,
  cardSubtitle,
  title,
  bodyText
} from "assets/jss/material-dashboard-pro-react.jsx";

import imagesStyles from "assets/jss/material-dashboard-pro-react/cardImagesStyles.jsx";

import modalStyle from "assets/jss/material-dashboard-pro-react/modalStyle.jsx";

import hoverCardStyle from "assets/jss/material-dashboard-pro-react/hoverCardStyle.jsx";

const dashboardStyle = theme => ({
  ...modalStyle(theme),
  ...hoverCardStyle,
  ...imagesStyles,
  cardImg: {
    height: "auto",
    width: "100%",
    display: "block",
    borderRadius: "5px"
  },
  feedImg: {
    width: "100%",
    display: "block",
    borderRadius: "5px",
    height: "auto"
  },
  imgDarken: {
    filter: "brightness(50%)"
  },
  bodyText: {
    ...bodyText,
    lineHeight: "20px",
    fontSize: "12px",
    marginTop: "5vh"
  },
  feedBody: {
    ...bodyText,
    margin: "0px 0px -40px 0px"
  },
  primary: {
    backgroundColor: primaryColor,
    color: "#FFFFFF"
  },
  caseCards: {
    marginTop: "10px",
    marginBottom: "10px"
  },
  dangerIcon: {
    color: dangerColor
  },
  successIcon: {
    color: successColor
  },
  bigNumbers: {
    ...bigNumbers
  },
  tooltip,
  cardTitle: {
    ...cardTitle,
    color: "#fff",
    marginTop: "0px",
    marginBottom: "3px"
  },
  cardTitle2: {
    ...cardTitle,
    marginTop: "0px",
    marginBottom: "3px"
  },
  title: {
    ...title,
    marginBottom: "2px",
    marginTop: "2px"
  },
  cardSubtitle: {
    ...title,
    color: "#fff",
    marginTop: "0",
    marginBottom: "0"
  },
  cardIconTitle: {
    ...cardTitle,
    marginTop: "15px",
    marginBottom: "0px"
  },
  cardProductTitle: {
    ...cardTitle,
    marginTop: "0px",
    marginBottom: "3px",
    textAlign: "center"
  },
  cardCategory: {
    color: "#999999",
    fontSize: "14px",
    paddingTop: "10px",
    marginBottom: "0",
    marginTop: "0",
    margin: "0"
  },
  cardProductDesciprion: {
    textAlign: "center",
    color: "#999999"
  },
  stats: {
    color: "#999999",
    fontSize: "12px",
    lineHeight: "22px",
    display: "inline-flex",
    "& svg": {
      position: "relative",
      top: "4px",
      width: "16px",
      height: "16px",
      marginRight: "3px"
    },
    "& .fab,& .fas,& .far,& .fal,& .material-icons": {
      position: "relative",
      top: "4px",
      fontSize: "16px",
      marginRight: "3px"
    }
  },
  productStats: {
    paddingTop: "7px",
    paddingBottom: "7px",
    margin: "0"
  },
  successText: {
    color: successColor
  },
  upArrowCardCategory: {
    width: 14,
    height: 14
  },
  underChartIcons: {
    width: "17px",
    height: "17px"
  },
  price: {
    color: "inherit",
    "& h4": {
      marginBottom: "0px",
      marginTop: "0px"
    }
  },
  searchButton: {
    marginTop: "1rem"
  },
  searchIcon: {
    color: "#aaa"
  },
  searchWrapper: {
    padding: "10px"
  }
});

export default dashboardStyle;
