import React from 'react';
import ReactDOM from 'react-dom';
import { createBrowserHistory } from 'history';
import { Router, Route, Switch } from 'react-router-dom';
import logger from 'redux-logger';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { reduxFirestore, getFirestore } from 'redux-firestore';
import { reactReduxFirebase, getFirebase } from 'react-redux-firebase';
import { routerMiddleware } from 'connected-react-router';
import { ConnectedRouter } from 'connected-react-router';

import { login, updateUserData } from './store/actions/userActions';
import { displayError } from './store/actions/errorActions';
import { firebase } from './firebase/fbConfig';
import rootReducer from './store/reducers/rootReducer';
import indexRoutes from 'routes/index.jsx';
import fbConfig from './firebase/fbConfig';
import Store from './services/StoreService';
import { getUserTopTracks } from 'services/providerService';
import config from 'config';
import * as serviceWorker from './serviceWorker';

import 'assets/scss/material-dashboard-pro-react.css?v=1.4.0';

const hist = createBrowserHistory();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  rootReducer(hist),
  composeEnhancers(
    applyMiddleware(
      thunk.withExtraArgument({ getFirebase, getFirestore }),
      routerMiddleware(hist),
      logger
    ),
    reduxFirestore(fbConfig),
    reactReduxFirebase(fbConfig)
  )
);

const setUserData = async (auth) => {
  try {
    // The data from firebase auth
    const { uid, displayName, photoURL, email } = auth;
    // The data we need in our database
    const user = { uid, displayName, photoURL, email };
    // Get the user data from our database if already exist
    const data = await Store.getUserData(uid);
    if (!data) {
      await Store.addUser(user);
    }
    store.dispatch(login(data || user));
    return data || user;
  } catch (error) {
    console.log(error);
    store.dispatch(displayError('Could not retrieve your data'));
  }
};

const getUserProviderTopTracks = async (uid) => {
  try {
    const providerToptracks = await getUserTopTracks({ uid });
    // Add the top tracks to the app state
    store.dispatch(updateUserData({ providerToptracks }));
  } catch (error) {
    console.log(error);
    if (error.name === 'Unauthorized') {
      window.location.replace(`${config.appBaseUrl}allow-spotify-access`);
      return;
    }
    store.dispatch(displayError('Could not connect to Spotify'));
  }
};

const onAuthentication = async (fuser) => {
  try {
    const { pathname } = hist.location;
    // Set user data in app state and also in db if new user
    const { spotifyAuth } = await setUserData(fuser);
    // Because the spotify authentication flow redirects to those paths
    if (
      pathname.includes('/spotify-authorization') ||
      pathname.includes('/allow-spotify-access')
    )
      return;
    // Get the user's top tracks on their spotify
    // account if spotify access has been approved
    if (spotifyAuth && spotifyAuth.status === 'authorized') {
      await getUserProviderTopTracks(fuser.uid);
    }
    // Redirect to the action component so we can wait for the  user data to
    // be populated in the state before we decide where to redirect the user
    hist.push({ pathname: '/redirect', redirectTo: pathname });
  } catch (error) {
    console.log(error);
    store.dispatch(displayError('Something went wrong.'));
  }
};

firebase.auth().onAuthStateChanged((fuser) => {
  if (fuser) {
    onAuthentication(fuser);
  } else {
    hist.push('/pages/login-page');
  }
});

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={hist}>
      <Router history={hist}>
        <Switch>
          {indexRoutes.map((prop, key) => {
            return (
              <Route path={prop.path} component={prop.component} key={key} />
            );
          })}
        </Switch>
      </Router>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
