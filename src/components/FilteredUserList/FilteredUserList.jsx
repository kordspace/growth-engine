import React from 'react';
import PropTypes from 'prop-types';

import Tooltip from '@material-ui/core/Tooltip';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Avatar from '@material-ui/core/Avatar';
import ListItemText from '@material-ui/core/ListItemText';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';

export const FilteredUserList = ({
  list,
  followUser,
  unFollowUser,
  isAlreadyFollowed,
}) => (
  <List
    dense
    component="nav"
    aria-labelledby="user-list"
    style={{
      backgroundColor: '#67898C',
      boxShadow: '0px 0px 15px -5px rgba(0,0,0,0)',
      width: '90%',
      margin: '0 10px 0 18px',
    }}
  >
    {list.map((item, i) => (
      <ListItem key={`${item.displayName}_${i}`}>
        <ListItemAvatar>
          <Avatar alt={item.displayName} src={item.avatar} />
        </ListItemAvatar>
        <ListItemText primary={item.displayName} style={{ color: '#FFFFFF' }} />
        <ListItemSecondaryAction>
          {isAlreadyFollowed(item.id) ? (
            <Tooltip title="Unfollow">
              <CloseIcon onClick={() => unFollowUser(item.id)} />
            </Tooltip>
          ) : (
            <Tooltip title="Follow">
              <AddIcon onClick={() => followUser(item)} />
            </Tooltip>
          )}
        </ListItemSecondaryAction>
      </ListItem>
    ))}
  </List>
);

FilteredUserList.propTypes = {
  list: PropTypes.arrayOf(PropTypes.object).isRequired,
  followUser: PropTypes.func.isRequired,
  unFollowUser: PropTypes.func.isRequired,
  isAlreadyFollowed: PropTypes.func.isRequired,
};

export default FilteredUserList;
