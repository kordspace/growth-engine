import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import Style from 'assets/jss/material-dashboard-pro-react/views/profileStyle.jsx';

export const UserBioSummary = ({
  classes,
  data: { bio, displayName, firstName, lastName, username, createdAt },
}) => (
  <div>
    <h3
      style={{
        fontSize: '22px',
        margin: '48px 20px -24px 30px',
      }}
      className={classes.body}
    >
      {firstName && lastName ? (
        <span>
          {' '}
          {firstName.toUpperCase()} <strong>{lastName.toUpperCase()}</strong>
        </span>
      ) : displayName ? (
        <strong>{displayName.toUpperCase()}</strong>
      ) : username ? (
        <strong>{username.toUpperCase()}</strong>
      ) : (
        'Missing name'
      )}
    </h3>
    {createdAt && (
      <h5
        style={{ fontSize: '18px', margin: '25px 20px 0px 30px' }}
        className={classes.body}
      >
        Joined {createdAt}
      </h5>
    )}
    {bio && (
      <p style={{ margin: '0px 20px 0px 30px' }} className={classes.body}>
        {bio}
      </p>
    )}
  </div>
);

UserBioSummary.propTypes = {
  data: PropTypes.shape({
    bio: PropTypes.string,
    lastName: PropTypes.string,
    firstName: PropTypes.string,
    displayName: PropTypes.string,
    dateCreated: PropTypes.string.isRequired,
  }).isRequired,
  classes: PropTypes.object.isRequired,
};

export default withStyles(Style)(UserBioSummary);
