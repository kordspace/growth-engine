import React from 'react';
import PropTypes from 'prop-types';

import ProfileUpload from 'components/CustomUpload/profilePicUpload';
import Button from 'components/CustomButtons/Button.jsx';
import Transition from 'components/Transition';

import CloseIcon from '@material-ui/icons/Close';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import withStyle from '@material-ui/core/styles/withStyles';
import Style from 'assets/jss/material-dashboard-pro-react/views/profileStyle.jsx';

export const ModalUpdateProfilePhoto = ({
  uid,
  user,
  open,
  classes,
  profilePhoto,
  handleModalClose,
}) => (
  <Dialog
    classes={{
      root: classes.modalRoot,
      paper: classes.modal + ' ' + classes.modalSmall,
    }}
    open={open}
    TransitionComponent={Transition}
    keepMounted
    onClose={handleModalClose}
    aria-labelledby="shipment-modal-slide-title"
    aria-describedby="shipment-modal-slide-description"
  >
    <DialogTitle
      id="shipment-modal-slide-title"
      disableTypography
      className={classes.modalHeader}
    >
      <Button
        simple
        className={classes.modalCloseButton}
        key="close"
        aria-label="Close"
        onClick={handleModalClose}
      >
        {' '}
        <CloseIcon className={classes.modalClose} />
      </Button>
      <h2 className={classes.modalTitle}>UPDATE PROFILE PHOTO</h2>
    </DialogTitle>
    <DialogContent
      id="shipment-modal-slide-description"
      className={classes.modalBody}
    >
      {/* FORM */}
      <ProfileUpload
        currentPic={profilePhoto}
        displayName={user.displayName}
        uid={uid}
      />
      <div
        style={{
          textAlign: 'center',
        }}
      >
        <Button
          className={classes.ltrButtonWhite}
          key="close"
          aria-label="Close"
          onClick={handleModalClose}
        >
          CANCEL
        </Button>
      </div>
    </DialogContent>
  </Dialog>
);

ModalUpdateProfilePhoto.propTypes = {
  uid: PropTypes.string.isRequired,
  user: PropTypes.object.isRequired,
  open: PropTypes.bool.isRequired,
  classes: PropTypes.object.isRequired,
  profilePhoto: PropTypes.string.isRequired,
  handleModalClose: PropTypes.func.isRequired,
};

export default withStyle(Style)(ModalUpdateProfilePhoto);
