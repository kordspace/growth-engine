import React from "react";
import { connect } from "react-redux";
import compose from "recompose/compose";

import PropTypes from "prop-types";

import axios from "axios";
import { db } from "firebase/fbConfig.js";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";

// @material-ui/icons
import Face from "@material-ui/icons/Face";
import Email from "@material-ui/icons/Email";
// import LockOutline from "@material-ui/icons/LockOutline";

// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardFooter from "components/Card/CardFooter.jsx";

import styles from "assets/jss/material-dashboard-pro-react/components/CreateCaseFormStyle.jsx";

class Form extends React.Component {
  state = {
    comment: "",
    messageSent: false,
    messageError: false
  };
  constructor(props) {
    super(props);
    // we use this to make the card to appear after the page has been rendered
    this.state = {
      cardAnimaton: "cardHidden"
    };
  }
  componentDidMount() {
    // we add a hidden class to the card and after 700 ms we delete it and the transition appears
    this.timeOutFunction = setTimeout(
      function() {
        this.setState({ cardAnimaton: "" });
      }.bind(this),
      700
    );
  }
  componentWillUnmount() {
    clearTimeout(this.timeOutFunction);
    this.timeOutFunction = null;
  }
  handleChange = (e, uid, id) => {
    this.setState({
      [e.target.id]: e.target.value,
      uid: uid,
      id: id
    });
    console.log(this.state);
  };
  handleSubmit = e => {
    e.preventDefault();
    console.log(this.state);
    // update firestore document with PENDING and comment
    db.collection("TEST-users")
      .doc(this.state.uid)
      .collection("threads")
      .doc(this.state.id)
      .collection("messages")
      .doc()
      .set(
        {
          uid: this.state.uid,
          message: this.state.message,
          createdTime: new Date()
        },
        { merge: true }
      )
      .then(() => {
        db.collection("TEST-users")
          .doc(this.state.id)
          .collection("threads")
          .doc(String(this.state.uid))
          .collection("messages")
          .doc()
          .set(
            {
              uid: this.state.uid,
              message: this.state.message,
              createdTime: new Date()
            },
            { merge: true }
          )
          .then(() => {
            console.log(``);
            this.setState({
              messageSent: true,
              message: ""
            });
            console.log(e);
          })
          .catch(err => {
            console.log(`${err}`);
          });
      })
      .catch(err => {
        console.log(`${err}`);
      });
  };

  resetRequest = () => {
    window.location.reload();
  };

  render() {
    const { classes, modalClose, authUser, blogID, uid, id } = this.props;
    //console.log(uid + " " + id)
    // reset modal when closed
    //if (this.state.messageSent) { this.resetRequest() }
    //console.log(this.state)
    return (
      <div>
        <div className={classes.formContainer}>
          <form onSubmit={this.handleSubmit}>
            <GridContainer justify="center">
              <GridItem xs={11}>
                {this.state.messageError ? (
                  <h5 className={classes.bodyText}>There Has Been An Error</h5>
                ) : null}

                <CustomInput
                  labelText="Type something"
                  id="message"
                  formControlProps={{
                    fullWidth: true
                  }}
                  inputProps={{
                    required: true,
                    type: "text",
                    value: this.state.message,
                    multiline: false,
                    rows: 1,
                    onChange: event => this.handleChange(event, uid, id)
                  }}
                />
              </GridItem>
            </GridContainer>
            <Button
              type="submit"
              color="primary"
              style={{
                marginLeft: "10px",
                marginBottom: "10px"
              }}
            >
              SUBMIT
            </Button>
          </form>
        </div>
      </div>
    );
  }
}

Form.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  //console.log(state)
  return {
    AuthBool: !!state.auth.user,
    authUser: state.auth.user,
    user: state.profile.user
  };
};

export default compose(
  connect(mapStateToProps),
  withStyles(styles)
)(Form);
