import React from "react";
import PropTypes from "prop-types";

import axios from "axios";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";

// @material-ui/icons
import Face from "@material-ui/icons/Face";
import Email from "@material-ui/icons/Email";
// import LockOutline from "@material-ui/icons/LockOutline";

// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardFooter from "components/Card/CardFooter.jsx";

import styles from "assets/jss/material-dashboard-pro-react/components/ActionSubmitFormStyle.jsx";

class Form extends React.Component {
  state = {
    name: "",
    department: "",
    issue: "",
    messageSent: false,
    messageError: false
  };
  constructor(props) {
    super(props);
    // we use this to make the card to appear after the page has been rendered
    this.state = {
      cardAnimaton: "cardHidden"
    };
  }
  componentDidMount() {
    // we add a hidden class to the card and after 700 ms we delete it and the transition appears
    this.timeOutFunction = setTimeout(
      function() {
        this.setState({ cardAnimaton: "" });
      }.bind(this),
      700
    );
  }
  componentWillUnmount() {
    clearTimeout(this.timeOutFunction);
    this.timeOutFunction = null;
  }
  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    this.sendMessage();
  };

  sendMessage = () => {
    let formId = "1FAIpQLScdfTZgxG1Kr-pJedt2klpHoaQcdR3raszyTdxto0MJKOfUpw";
    let url = `https://docs.google.com/forms/d/e/${formId}/formResponse?usp=pp_url`;
    url += `&entry.1069438332=${this.state.name}`;
    url += `&entry.870431466=${this.state.department}`;
    url += `&entry.2119411844=${this.state.issue}&submit=Submit`;

    axios
      .get(url, {
        data: {},
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        }
      })
      .then(res => console.log(res))
      .catch(err => {
        // data sent even with cors error
        console.log(err);
        this.setState({
          name: "",
          department: "",
          issue: "",
          messageSent: true
        });
      });
  };

  resetRequest = () => {
    this.setState({
      name: "",
      department: "",
      issue: "",
      messageSent: false
    });
  };

  render() {
    const { classes, modalClose } = this.props;
    console.log(modalClose);
    // reset modal when closed
    if (modalClose && this.state.messageSent) {
      this.resetRequest();
    }

    return (
      <div>
        {this.state.messageSent ? (
          <GridContainer justify="center">
            <GridItem xs={11}>
              <h4 className={classes.textGreen}>
                Your Message Has Been Sent! An IT rep will contact you shortly.
              </h4>
            </GridItem>
          </GridContainer>
        ) : (
          <div className={classes.formContainer}>
            <form onSubmit={this.handleSubmit}>
              <GridContainer justify="center">
                <GridItem xs={11}>
                  <h5 className={classes.bodyText}>
                    Submit a help request and we'll respond as soon as possible
                  </h5>
                  {this.state.messageError ? (
                    <h5 className={classes.bodyText}>
                      There Has Been An Error
                    </h5>
                  ) : null}
                  <CustomInput
                    labelText="Name"
                    id="name"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      required: true,
                      type: "text",
                      onChange: event => this.handleChange(event)
                    }}
                  />
                  <CustomInput
                    labelText="Department"
                    id="department"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      required: true,
                      type: "text",
                      onChange: event => this.handleChange(event)
                    }}
                  />
                  <CustomInput
                    labelText="Issue"
                    id="issue"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      required: true,
                      type: "text",
                      multiline: true,
                      rows: 5,
                      onChange: event => this.handleChange(event)
                    }}
                  />
                </GridItem>
              </GridContainer>
              <Button type="submit" className={classes.ltrButtonTeal}>
                Save Changes
              </Button>
            </form>
          </div>
        )}
      </div>
    );
  }
}

Form.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Form);
