import React from 'react';
import PropTypes from 'prop-types';
import Hidden from '@material-ui/core/Hidden';
import Button from 'components/CustomButtons/Button.jsx';
import BeenhereOutlinedIcon from '@material-ui/icons/BeenhereOutlined';

export const EditProfileButton = ({ onClick }) => (
  <div>
    {/* desktop */}
    <Hidden xsDown implementation="css">
      <Button
        style={{
          margin: '20% 0% 0% 23%',
          borderRadius: '50px',
          boxShadow: 'none',
          backgroundColor: '#009874',
        }}
        color="primary"
        onClick={onClick}
      >
        <b>
          Edit Profile
          <BeenhereOutlinedIcon
            style={{
              margin: '-5px 0px -5px 15px',
              width: '17px',
              height: '17px',
            }}
          />
        </b>
      </Button>
    </Hidden>
    {/* mobile */}
    <Hidden smUp implementation="css">
      <Button
        style={{
          margin: '-56% 0% 4% 20%',
          borderRadius: '50px',
          boxShadow: 'none',
          backgroundColor: '#009874',
        }}
        color="primary"
        onClick={onClick}
      >
        <b>
          Edit Profile
          <BeenhereOutlinedIcon
            style={{
              margin: '-5px 0px -5px 15px',
              width: '17px',
              height: '17px',
            }}
          />
        </b>
      </Button>
    </Hidden>
  </div>
);

EditProfileButton.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default EditProfileButton;
