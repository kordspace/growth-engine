import React from 'react';
import { Link } from 'react-router-dom';

export const BuyEarnTokensButton = () => {
  return (
    <div
      style={{
        backgroundColor: '#21384a',
        height: '20vh',
        width: '20vw',
        color: '#fff',
      }}>
      Not enough tokens available
      <button
        style={{
          borderRadius: '10px',
          backgroundColor: '#009874',
        }}>
        <p
          style={{
            fontSize: '22px',
            fontWeight: '600',
            color: '#fff',
            marginTop: '10px',
            width: '50px',
          }}>
          <a href={'https://www.arcq.app/shop/'}>Shop</a>
        </p>
      </button>
      <Link to={{pathname: '/invite'}} >
        <button
          style={{
            borderRadius: '10px',
            backgroundColor: '#faeb60',
          }}>
          <p
            style={{
              fontSize: '22px',
              fontWeight: '600',
              color: '#fff',
              marginTop: '10px',
              width: '50px',
            }}>
            Earn Tokens
          </p>
        </button>
      </Link>
    </div>
  );
};

export default BuyEarnTokensButton;
