import React from 'react';
import { Link } from 'react-router-dom';
import Hidden from '@material-ui/core/Hidden';
import Button from 'components/CustomButtons/Button.jsx';
import SupervisedUserCircleIcon from '@material-ui/icons/SupervisedUserCircle';

export const FollowersButton = () => (
  <div>
    {/* desktop */}
    <Hidden xsDown implementation='css'>
      <Link to={{ pathname: '/followers' }}>
        <Button
          style={{
            margin: '20% 0% 0% 40%',
            borderRadius: '50px',
            boxShadow: 'none',
            backgroundColor: '#6E9290',
          }}
          color='primary'
        >
          <b>
            Followers
            <SupervisedUserCircleIcon
              style={{
                margin: '-5px 0px -5px 15px',
                width: '17px',
                height: '17px',
              }}
            />
          </b>
        </Button>
      </Link>
    </Hidden>
    {/* mobile */}
    <Hidden smUp implementation='css'>
      <Link to={{ pathname: '/followers' }}>
        <Button
          style={{
            margin: '-60% 0% 0% 33%',
            borderRadius: '50px',
            boxShadow: 'none',
            backgroundColor: '#6E9290',
          }}
          color='primary'
        >
          <b>
            Followers
            <SupervisedUserCircleIcon
              style={{
                margin: '-5px 0px -5px 15px',
                width: '17px',
                height: '17px',
              }}
            />
          </b>
        </Button>
      </Link>
    </Hidden>
  </div>
);

export default FollowersButton;
