import React from 'react';
import Slide from '@material-ui/core/Slide';

const Transition = (props) => <Slide direction="down" {...props} />;

export default Transition;
