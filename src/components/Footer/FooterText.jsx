import React from "react";
import PropTypes from "prop-types";

export const FooterText = props => (
  <span>
    .
    {/*&copy; {1900 + new Date().getYear()}{" "}
    <a href="http://www.kord.space/" className={props.anchor}>
      {props.rtlActive ? "توقيت الإبداعية" : "Kordspace"}
    </a>
    {props.rtlActive
      ? ", مصنوعة مع الحب لشبكة الإنترنت أفضل"
      : ""}
    &nbsp;
    {`(${process.env.REACT_APP_GIT_SHA})`}*/}
  </span>
);

FooterText.propTypes = {
  anchor: PropTypes.string.isRequired,
  rtlActive: PropTypes.bool
};
