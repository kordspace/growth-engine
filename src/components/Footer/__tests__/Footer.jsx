import React from "react";
import { shallow } from "enzyme";
import Footer from "../Footer";
import toJson from "enzyme-to-json";

describe("FooterText component", () => {
  it("renders correctly", () => {
    const props = { classes: {}, anchor: "anchor-class", rtlEnabled: false };
    const wrapper = shallow(<Footer {...props} />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
