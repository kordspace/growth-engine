import React from "react";
import { shallow } from "enzyme";
import { FooterText } from "../FooterText";
import toJson from "enzyme-to-json";

describe("FooterText component", () => {
  beforeAll(() => {
    process.env.REACT_APP_GIT_SHA = "123abc";
  });

  afterAll(() => delete process.env.REACT_APP_GIT_SHA);

  it("renders correctly", () => {
    const props = { anchor: "anchor-class", rtlEnabled: false };
    const wrapper = shallow(<FooterText {...props} />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
