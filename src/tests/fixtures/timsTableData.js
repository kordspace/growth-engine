//Formatted for use with Table component
export const tableData = {
  tableHead: ["userName", "score"],
  tableData: [
    ["Emily R. Shelton", 92],
    ["Justin Oberg", 70],
    ["Matt Cahn ltl.ai", 30],
    ["Justin Oberg", 12],
    ["jack collins", 10]
  ]
};

export const firestoreUploadTableData = [
  { score: 92, userName: "Emily R. Shelton" },
  { score: 70, userName: "Justin Oberg" },
  { score: 30, userName: "Matt Cahn ltl.ai" },
  { score: 12, userName: "Justin Oberg" },
  { score: 10, userName: "jack collins" }
];

//Formatted for use with ReactTable component
export const reactTableData = {
  columns: [
    {
      Header: "ACTION ID",
      accessor: "actionID",
      show: false
    },
    {
      Header: "TITLE",
      accessor: "title"
    },
    {
      Header: "BODY",
      accessor: "description",
      show: false
    },
    {
      Header: "DUE",
      accessor: "dueDate"
    },
    {
      Header: "DAYS DUE",
      accessor: "days",
      show: false
    },
    {
      Header: "POINTS",
      accessor: "points"
    },
    {
      Header: "STATUS",
      accessor: "status"
    },
    {
      Header: "",
      accessor: "actions",
      sortable: false,
      filterable: false
    }
  ],
  dataRows: [
    [
      "1",
      "Share Post on Facebook",
      "Share Post on Facebook",
      "June 12",
      "14 days",
      5,
      "assigned"
    ],
    [
      "2",
      "Door knocking",
      "Door knocking",
      "June 12",
      "14 days",
      40,
      "assigned"
    ],
    [
      "3",
      "Host House Party",
      "Host House Party",
      "June 12",
      "14 days",
      100,
      "pending"
    ],
    [
      "4",
      "Phone banking",
      "Phone banking",
      "June 12",
      "14 days",
      40,
      "pending"
    ],
    [
      "5",
      "Submit Chatbot Questions",
      "Submit Chatbot Questions",
      "June 12",
      "14 days",
      10,
      "confirmed"
    ]
  ]
};

export const firestoreUploadReactTableData = [
  {
    actionID: "1",
    days: "14 days",
    description: "Share Post on Facebook",
    dueDate: "June 12",
    points: 5,
    status: "assigned",
    title: "Share Post on Facebook"
  },
  {
    actionID: "2",
    days: "14 days",
    description: "Door knocking",
    dueDate: "June 12",
    points: 40,
    status: "assigned",
    title: "Door knocking"
  },
  {
    actionID: "3",
    days: "14 days",
    description: "Host House Party",
    dueDate: "June 12",
    points: 100,
    status: "pending",
    title: "Host House Party"
  },
  {
    actionID: "4",
    days: "14 days",
    description: "Phone banking",
    dueDate: "June 12",
    points: 40,
    status: "pending",
    title: "Phone banking"
  },
  {
    actionID: "5",
    days: "14 days",
    description: "Submit Chatbot Questions",
    dueDate: "June 12",
    points: 10,
    status: "confirmed",
    title: "Submit Chatbot Questions"
  }
];
