export default [
  {
    actionId: "2nELckKumWgxwO0QXTPc",
    title: "Like post on Facebook",
    description: "Link to post",
    dueDate: "June 8th",
    days: 1,
    points: 5,
    status: "confirmed",
    comment: "Update comment"
  },
  {
    actionId: "3by8SqmcoRJpgE77lT1e",
    title: "Knock 50 Doors",
    description: "In Boulder",
    dueDate: "June 9th",
    days: 2,
    points: 25,
    status: "confirmed",
    comment: "Update from mobile"
  },
  {
    actionId: "5EYItNFbRLq9415YLZm2",
    title: "Knock 50 doors",
    description: "Area 42",
    dueDate: "June 10th",
    days: 2,
    points: 20,
    status: "confirmed",
    comment: "Knocked some doors!"
  }
];
