import actionsReducer, { initState } from "store/reducers/actionsReducer.jsx";

import dataRows from "tests/fixtures/actionDataRows.js";
import leaderboardTableData from "tests/fixtures/leaderboardTableData.js";

test("should setup default actions values", () => {
  const state = actionsReducer(undefined, { type: "@@INIT" });
  expect(state).toEqual(initState);
});

test("should set actions dataRows and flip actions dataLoaded to True", () => {
  const action = {
    type: "SET_ACTIONS_LIST",
    dataRows
  };

  const state = actionsReducer(initState, action);

  expect(state).toEqual({
    ...initState,
    actions: {
      ...initState.actions,
      dataLoaded: true,
      dataRows
    }
  });
});

test("should set users tableData, userIndex, and flip users dataLoaded to True", () => {
  const action = {
    type: "SET_USERS_LIST",
    userIndex: 1,
    tableData: leaderboardTableData
  };

  const state = actionsReducer(initState, action);

  expect(state).toEqual({
    ...initState,
    users: {
      ...initState.users,
      dataLoaded: true,
      userIndex: 1,
      dataRows: leaderboardTableData
    }
  });
});

test("should set actions data loaded value to false", () => {
  const currentState = {
    ...initState,
    actions: {
      ...initState.actions,
      dataLoaded: true
    }
  };
  const state = actionsReducer(currentState, { type: "RESET_ACTIONS_DATA" });
  expect(state).toEqual(initState);
});
