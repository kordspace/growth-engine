import authReducer, { initState } from "store/reducers/authReducer.jsx";

import userObj from "tests/fixtures/userObj.js";

test("should setup default auth values", () => {
  const state = authReducer(undefined, { type: "@@INIT" });
  expect(state).toEqual(initState);
});

test("should setup user object in state", () => {
  const action = {
    type: "LOGIN",
    user: userObj
  };

  const state = authReducer(initState, action);

  expect(state).toEqual({
    ...initState,
    user: userObj
  });
});

test("should set authError string in state", () => {
  const state = authReducer(initState, { type: "LOGIN_ERROR" });

  expect(state).toEqual({
    ...initState
  });
});

test("should reset state to initState on logout", () => {
  const loginState = {
    ...initState,
    user: userObj
  };

  const state = authReducer(loginState, { type: "LOGOUT" });

  expect(state).toEqual(initState);
});
