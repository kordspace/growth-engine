import {
  startSetUser,
  setUser,
  updateUser
} from "store/actions/profileActions.jsx";

import userObj from "tests/fixtures/userObj";

test("should setup setUser action object", () => {
  const action = setUser(userObj);
  expect(action).toEqual({
    type: "SET_USER",
    userObj
  });
});
