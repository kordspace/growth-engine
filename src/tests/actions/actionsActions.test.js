import {
  setFirstAction,
  startSetActionsList,
  setActionsList,
  startSetUsersList,
  setUsersList,
  setUserScore,
  resetActionsData
} from "store/actions/actionsActions.jsx";

import actionDataRows from "tests/fixtures/actionDataRows.js";
import leaderboardTableData from "tests/fixtures/leaderboardTableData.js";

test("should setup setActionsList action object", () => {
  const action = setActionsList(actionDataRows);
  expect(action).toEqual({
    type: "SET_ACTIONS_LIST",
    dataRows: actionDataRows
  });
});

test("should setup setUsersList action object", () => {
  const action = setUsersList(leaderboardTableData, 2);
  expect(action).toEqual({
    type: "SET_USERS_LIST",
    tableData: leaderboardTableData,
    userIndex: 2
  });
});

test("should setup resetActions action object", () => {
  const action = resetActionsData();
  expect(action).toEqual({
    type: "RESET_ACTIONS_DATA"
  });
});
