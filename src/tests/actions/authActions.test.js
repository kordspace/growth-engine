import {
  emailSignIn,
  loginError,
  passwordReset,
  startGoogleLogin,
  login,
  startLogout,
  logout
} from "store/actions/authActions.jsx";

test("should setup loginError action object", () => {
  const action = loginError("test error");
  expect(action).toEqual({ error: "test error", type: "LOGIN_ERROR" });
});

test("should generate login action object for blank user", () => {
  const action = login();
  expect(action).toEqual({
    type: "LOGIN",
    user: {}
  });
});

test("should generate login action object with user", () => {
  const user = {
    name: "test user",
    email: "test@test.com"
  };
  const action = login(user);
  expect(action).toEqual({
    type: "LOGIN",
    user
  });
});

test("should generate logout action object", () => {
  const action = logout();
  expect(action).toEqual({ type: "LOGOUT" });
});
