import {
  addFieldToDocument,
  updateDocumentField,
  tableToFirestoreObjects,
  reactTableToFirestoreObjects,
  updateFirestoreWithOneObject,
  getFirestoreData
} from "store/actions/firestoreActions.jsx";

import {
  tableData,
  firestoreUploadTableData,
  reactTableData,
  firestoreUploadReactTableData
} from "tests/fixtures/timsTableData.js";

test("should convert Table Component formatted table data into array of objects for firestore upload", () => {
  const firestoreObject = tableToFirestoreObjects(tableData);
  expect(firestoreObject).toEqual(firestoreUploadTableData);
});

test("should convert ReactTable Component formatted table data into array of objects for firestore upload", () => {
  const firestoreObject = reactTableToFirestoreObjects(reactTableData);
  expect(firestoreObject).toEqual(firestoreUploadReactTableData);
});
