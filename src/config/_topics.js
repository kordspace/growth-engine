export default [
  {
    category: 'Intent Category Types',
    topics: [
      'Informed',
      'Expressed',
      'Requested',
      'Response',
      'Opinion',
      'Recall',
      'Salutations',
      'Contextual',
    ],
  },
  {
    category: 'Conversational Category',
    topics: [
      'Technology',
      'Spirituality',
      'Heritage',
      'Self-Discovery',
      'Life Legacy',
      'Favorites',
      'Ethics',
      'Health',
      'Politics',
      'Memories',
    ],
  },
  {
    category: 'Intent Category Archive',
    topics: [
      'Informed',
      'Expressed',
      'Requested',
      'Response',
      'Deceived',
      'Dismissed',
      'Recall',
      'Salutations',
      'Contextual',
    ],
  },
];
