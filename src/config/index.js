export default {
  models: [
    { model: 'growth_engine', type: 'Growth Engine' },
  ],
  picAllowedExt: ['jpg', 'jpeg', 'png'],
  // _TODO: why do we need "%2F"?
  avatarStoragePath:
    'https://firebasestorage.googleapis.com/v0/b/growth-engine-295114.appspot.com/o/avatars%2F',
  avatarStorageFolder: 'avatars',
  defaultOracleResponseLength: 150,
};
