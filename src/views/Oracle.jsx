import React, { useState, useEffect } from 'react';
import range from 'lodash/range';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Loader from 'components/Loader';
import GridContainer from 'components/Grid/GridContainer.jsx';
import GridItem from 'components/Grid/GridItem.jsx';
import { getGPTModels } from 'store/actions/gptActions';
import { updateUser } from 'store/actions/profileActions';
import { displayError } from 'store/actions/errorActions';
import { capitalize } from 'utils';
import { firebase } from 'firebase/fbConfig';
import config from 'config';
import { setScreenName } from 'store/actions/screenNameActions';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import LoaderGif from 'assets/img/loader-dots.gif';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';

const { defaultOracleResponseLength } = config;
const callable = firebase.functions().httpsCallable('generateText');

const Oracle = ({ uid, type, gptModels, getGPTModels, setScreenName }) => {
  let model;
  const [open, setOpen] = useState(false);
  const [selectedSample, setSelectedSample] = useState('');
  const [length, setLength] = useState(150);
  const [results, setResults] = useState([]);
  const [prompt, setPrompt] = useState('');
  const [callingModel, setCallingModel] = useState(false);
  const [nbOfSamples, setNbOfSamples] = useState(1);

  const submit = async () => {
    try {
      setResults([]);
      // Simply set the value and exit if it's not a submit event
      if (!prompt) return;
      // Get oracles's response
      setCallingModel(true);
      const response = await callable({
        text: prompt.trim(),
        model: model.name,
        length: length || defaultOracleResponseLength,
        samples: nbOfSamples,
        parallelComputation: false,
      });
      setCallingModel(false);
      // Reset inputs
      setPrompt('');
      setNbOfSamples(1);
      const { error, results } = response.data;
      if (error) {
        throw new Error(error);
      }
      console.log(results);
      setResults(results);
      setCallingModel(false);
    } catch (error) {
      setCallingModel(false);
      console.error(error.toString());
    }
  };

  useEffect(() => {
    if (!gptModels) {
      getGPTModels();
    } else if (uid) {
      setScreenName(capitalize(model.name));
    }
  }, [uid, gptModels]);

  if (!uid || !gptModels) return <Loader />;

  model = gptModels.find((e) => e.type === type);

  if (!model) return <Redirect to='/not-found' />;

  return (
    <div>
      <GridContainer justify='center'>
        <GridItem xs={12} style={{ textAlign: 'center', marginTop: '150px' }}>
          <TextareaAutosize
            value={prompt}
            aria-label='minimum height'
            rowsMin={3}
            placeholder='Enter prompt'
            onChange={(e) => setPrompt(e.target.value)}
            style={{
              width: '225px',
              padding: '10px',
              borderRadius: '5px',
              border: '1px solid #CCC',
            }}
          />
        </GridItem>
      </GridContainer>
      <GridContainer justify='center' style={{ marginTop: '30px' }}>
        <GridItem>
          <FormControl variant='outlined'>
            <InputLabel id='demo-simple-select-outlined-label'>Nb</InputLabel>
            <Select
              labelId='demo-simple-select-outlined-label'
              id='demo-simple-select-outlined'
              value={nbOfSamples}
              onChange={(e) => setNbOfSamples(e.target.value)}
              label='samples'
            >
              {range(1, 10).map((e) => (
                <MenuItem value={e}>
                  {e} sample{e > 1 ? 's' : ''}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </GridItem>
        <GridItem>
          <TextField
            id='outlined-basic'
            label='Length'
            value={length}
            variant='outlined'
            onChange={(e) => setLength(e.target.value.trim())}
            defaultValue='150'
            style={{ width: '90px' }}
          />
        </GridItem>
      </GridContainer>
      <GridContainer>
        <GridItem
          xs={12}
          style={{
            textAlign: 'center',
            marginTop: '30px',
          }}
        >
          {callingModel ? (
            <div>
              <img src={LoaderGif} style={{ width: '30px' }} alt='...' />
            </div>
          ) : (
            <Button
              variant='contained'
              color='primary'
              disableElevation
              onClick={submit}
            >
              Submit
            </Button>
          )}
        </GridItem>
      </GridContainer>
      {!!results.length && (
        <div>
          <GridContainer justify='center' style={{ marginTop: '20px' }}>
            <GridItem>Click to see your samples</GridItem>
          </GridContainer>
          <GridContainer justify='center' style={{ marginTop: '20px' }}>
            {results.map((e, i) => (
              <GridItem xs={1}>
                <div
                  style={{
                    width: '50px',
                    height: '50px',
                    paddingTop: '15px',
                    textAlign: 'center',
                    fontSize: '20px',
                    background: '#DBDBDB',
                    borderRadius: '50px',
                    cursor: 'pointer',
                  }}
                  onClick={() => {
                    setOpen(true);
                    setSelectedSample(e);
                  }}
                >
                  {i + 1}
                </div>
              </GridItem>
            ))}
          </GridContainer>
        </div>
      )}
      <Dialog
        open={open}
        onClose={() => {
          setOpen(false);
          setSelectedSample('');
        }}
      >
        <div
          style={{
            padding: '20px',
          }}
        >
          {selectedSample}
        </div>
      </Dialog>
    </div>
  );
};

Oracle.propTypes = {
  uid: PropTypes.string,
  type: PropTypes.string.isRequired,
  gptModels: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      type: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    })
  ),
  events: PropTypes.array,
  updateUser: PropTypes.func.isRequired,
  displayError: PropTypes.func.isRequired,
  getGPTModels: PropTypes.func.isRequired,
  setScreenName: PropTypes.func.isRequired,
};

const mapStateToProps = (state, ownProps) => {
  const { type } = ownProps.match.params;
  const { gptModels, profile } = state;
  const { uid, events } = profile || {};
  return { uid, events, type, gptModels };
};

export default connect(mapStateToProps, {
  updateUser,
  getGPTModels,
  displayError,
  setScreenName,
})(Oracle);
