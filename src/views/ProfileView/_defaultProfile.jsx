import React from 'react';
import { connect } from 'react-redux';
import compose from 'recompose/compose';
import { db, firebase } from 'firebase/fbConfig.js';

// react component for creating dynamic tables
import ReactTable from 'react-table';

// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
import Card from 'components/Card/Card.jsx';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Slide from '@material-ui/core/Slide';

// @material-ui/icons
import Assignment from '@material-ui/icons/Assignment';
import Dvr from '@material-ui/icons/Dvr';
import Favorite from '@material-ui/icons/Favorite';
import Check from '@material-ui/icons/Check';
import Close from '@material-ui/icons/Close';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Loader from 'assets/img/preloader.gif';

// core components
import GridContainer from 'components/Grid/GridContainer.jsx';
import GridItem from 'components/Grid/GridItem.jsx';
import Button from 'components/CustomButtons/Button.jsx';
import ProfileUpload from 'components/CustomUpload/profilePicUpload';
import CardBody from 'components/Card/CardBody.jsx';
import CardIcon from 'components/Card/CardIcon.jsx';
import CardHeader from 'components/Card/CardHeader.jsx';
import Table from 'components/Table/Table.jsx';

import { cardTitle } from 'assets/jss/material-dashboard-pro-react.jsx';
import Style from 'assets/jss/material-dashboard-pro-react/views/profileStyle';
import { Grid } from '@material-ui/core';

// Forms
import ProfileForm from 'components/Forms/profileForm.jsx';

// Assets
import DefaultProfile from 'assets/img/default-avatar.png';

// Firebase Redux Integration
import {
  tableToFirestoreObjects,
  updateFirestoreWithOneObject,
  updateFirestoreWithTableData,
  addFieldToDocument,
} from 'store/actions/firestoreActions';
import {
  startSetActionsList,
  startToggleCompleted,
  setUserScore,
} from 'store/actions/actionsActions';

const Transition = (props) => {
  return <Slide direction="down" {...props} />;
};

class Orders extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dataLoaded: false,
      updateProfileModal: false,
      updatePhotoModal: false,
    };
  }

  handleClickOpen = (modal) => {
    var x = [];
    x[modal] = true;
    this.setState({
      ...x,
    });
  };

  handleClose = (modal) => {
    var x = [];
    x[modal] = false;
    this.setState(x);
  };

  loadData = () => {
    this.setState({
      dataLoaded: true,
    });
  };

  render() {
    const { AuthBool, classes, user } = this.props;

    console.log(this.state);

    let profilePhoto = DefaultProfile;
    if (AuthBool) {
      // fixes freeze on logout
      if (user.dataLoaded) {
        user.profileURL
          ? (profilePhoto = user.profileURL)
          : // add link to profile photo to firestore
            (profilePhoto = this.props.authUser.photoURL
              ? this.props.authUser.photoURL
              : DefaultProfile);
        // add profile photo to firebase storage
        // update redux with link
      }
    }

    if (user.dataLoaded && !this.state.dataLoaded) {
      this.loadData();
    }
    return (
      <GridContainer justify="center">
        <GridItem xs={12} sm={10} md={6}>
          {this.state.dataLoaded ? (
            <GridContainer>
              <GridItem xs={12}>
                <h1 className={classes.title}>PROFILE</h1>
              </GridItem>
              <GridItem xs={10} sm={8} lg={4}>
                <div onClick={() => this.handleClickOpen('updatePhotoModal')}>
                  <img
                    className={classes.profilePic}
                    src={profilePhoto}
                    alt="User Photo"
                  />
                  <p className={classes.body}>Update Photo</p>
                </div>
              </GridItem>
              <GridItem xs={12} sm={6} lg={4}>
                <h4 className={classes.subTitle}>NAME:</h4>
                {this.props.user.firstName ? (
                  <p className={classes.body}>
                    {this.props.user.firstName}{' '}
                    {this.props.user.lastName ? this.props.user.lastName : null}
                  </p>
                ) : (
                  <p className={classes.body}>Click UPDATE to add your name</p>
                )}
                <h4 className={classes.subTitle}>USER NAME:</h4>
                <p className={classes.body}>{this.props.user.displayName}</p>
                <h4 className={classes.subTitle}>EMAIL:</h4>
                <p className={classes.body}>{this.props.user.email}</p>
                <h4 className={classes.subTitle}>PHONE:</h4>
                {this.props.user.phone ? (
                  <p className={classes.body}>{this.props.user.phone}</p>
                ) : (
                  <p className={classes.body}>Click UPDATE to add phone number</p>
                )}
              </GridItem>
              <GridItem xs={12} sm={6} lg={4}>
                <h4 className={classes.subTitle}>COUNTY:</h4>
                {this.props.user.county ? (
                  <p className={classes.body}>{this.props.user.county}</p>
                ) : (
                  <p className={classes.body}>click UPDATE to add your county</p>
                )}
                <h4 className={classes.subTitle}>PRECINCT:</h4>
                {this.props.user.precinct ? (
                  <p className={classes.body}>{this.props.user.precinct}</p>
                ) : (
                  <p className={classes.body}>
                    click UPDATE and select your county to view precinct dropdown.
                  </p>
                )}
                <h4 className={classes.subTitle}>GROUPS:</h4>
                {this.props.user.groups.length > 0 ? (
                  this.props.user.groups.map((group) => {
                    return (
                      <p className={classes.body} key={group}>
                        {group}
                      </p>
                    );
                  })
                ) : (
                  <p className={classes.body}>
                    click UPDATE to add yourself to groups
                  </p>
                )}
                <Button
                  color="primary"
                  onClick={() => this.handleClickOpen('updateProfileModal')}
                >
                  UPDATE
                </Button>
              </GridItem>
              {/* UPDATE PROFILE MODAL */}
              <Dialog
                classes={{
                  root: classes.modalRoot,
                  paper: classes.modal + ' ' + classes.modalSmall,
                }}
                open={this.state.updateProfileModal}
                TransitionComponent={Transition}
                keepMounted
                onClose={() => this.handleClose('updateProfileModal')}
                aria-labelledby="shipment-modal-slide-title"
                aria-describedby="shipment-modal-slide-description"
              >
                <DialogTitle
                  id="shipment-modal-slide-title"
                  disableTypography
                  className={classes.modalHeader}
                >
                  <Button
                    simple
                    className={classes.modalCloseButton}
                    key="close"
                    aria-label="Close"
                    onClick={() => this.handleClose('updateProfileModal')}
                  >
                    {' '}
                    <Close className={classes.modalClose} />
                  </Button>
                  <h2 className={classes.modalTitle}>EDIT PROFILE</h2>
                </DialogTitle>
                <DialogContent
                  id="shipment-modal-slide-description"
                  className={classes.modalBody}
                >
                  {/* FORM */}
                  {AuthBool ? (
                    <ProfileForm
                      user={this.props.user}
                      uid={this.props.authUser.uid}
                      modalOpen={this.state.updateProfileModal}
                    />
                  ) : null}
                  <Button
                    className={classes.ltrButtonWhite}
                    style={{
                      marginLeft: '10px',
                      marginTop: '10px',
                    }}
                    key="close"
                    aria-label="Close"
                    onClick={() => this.handleClose('updateProfileModal')}
                  >
                    CLOSE
                  </Button>
                </DialogContent>
              </Dialog>

              {/* UPDATE PROFILE PHOTO MODAL */}
              <Dialog
                classes={{
                  root: classes.modalRoot,
                  paper: classes.modal + ' ' + classes.modalSmall,
                }}
                open={this.state.updatePhotoModal}
                TransitionComponent={Transition}
                keepMounted
                onClose={() => this.handleClose('updatePhotoModal')}
                aria-labelledby="shipment-modal-slide-title"
                aria-describedby="shipment-modal-slide-description"
              >
                <DialogTitle
                  id="shipment-modal-slide-title"
                  disableTypography
                  className={classes.modalHeader}
                >
                  <Button
                    simple
                    className={classes.modalCloseButton}
                    key="close"
                    aria-label="Close"
                    onClick={() => this.handleClose('updatePhotoModal')}
                  >
                    {' '}
                    <Close className={classes.modalClose} />
                  </Button>
                  <h2 className={classes.modalTitle}>UPDATE PROFILE PHOTO</h2>
                </DialogTitle>
                <DialogContent
                  id="shipment-modal-slide-description"
                  className={classes.modalBody}
                >
                  {/* FORM */}
                  <ProfileUpload
                    currentPic={profilePhoto}
                    displayName={this.props.user.displayName}
                    uid={this.props.authUser.uid}
                  />
                  <div
                    style={{
                      textAlign: 'center',
                    }}
                  >
                    <Button
                      className={classes.ltrButtonWhite}
                      key="close"
                      aria-label="Close"
                      onClick={() => this.handleClose('updatePhotoModal')}
                    >
                      CANCEL
                    </Button>
                  </div>
                </DialogContent>
              </Dialog>
            </GridContainer>
          ) : (
  <div
    style={{
      background: '#000',
      position: 'absolute',
      width: '110%',
      marginLeft: '-7%',
      height: '100%',
    }}
  >
    {/* Desktop */}
      <Hidden xsDown implementation="css">
      <img
      style={{
        margin: '45% 0% 0% 41%',
        position: 'relative',
        width: '20%',
      }}
      src={LoaderGif}
      alt="..."
      />
      </Hidden>
      {/* Mobile */}
        <Hidden smUp implementation="css">
    <img
      style={{
        margin: '65% 0% 0% 41%',
        position: 'relative',
        width: '20%',
      }}
      src={LoaderGif}
      alt="..."
    />
    </Hidden>
  </div>
          )}
        </GridItem>
      </GridContainer>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    AuthBool: !!state.auth.user,
    authUser: state.auth.user,
    user: state.profile.user,
  };
};

// const mapDispatchToProps = (dispatch) => {
//   return {
//     setUserScore: (score) => dispatch(setUserScore(score))
//   }
// }

export default compose(
  connect(mapStateToProps),
  withStyles(Style)
)(Orders);
