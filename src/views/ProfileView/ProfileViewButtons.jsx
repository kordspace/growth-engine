import React from 'react';
import PropTypes from 'prop-types';
import GridContainer from 'components/Grid/GridContainer.jsx';
import GridItem from 'components/Grid/GridItem.jsx';
import FollowersButton from 'components/FollowersButton';
import EditProfileButton from 'components/EditProfileButton';

export const ProfileViewButtons = ({ openEditProfile }) => (
  <GridContainer justify='center' style={{ margin: '-14px -51px 0px' }}>
    <GridItem xs={6}>
      <FollowersButton />
    </GridItem>
    <GridItem xs={6}>
      <EditProfileButton onClick={openEditProfile} />
    </GridItem>
  </GridContainer>
);

ProfileViewButtons.propTypes = {
  openEditProfile: PropTypes.func.isRequired,
};

export default ProfileViewButtons;
