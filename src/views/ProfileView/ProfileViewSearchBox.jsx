import React from 'react';
import PropTypes from 'prop-types';
import GridContainer from 'components/Grid/GridContainer.jsx';
import GridItem from 'components/Grid/GridItem.jsx';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import SearchInput from 'components/SearchInput';
import FilteredUserList from 'components/FilteredUserList';

export const ProfileViewSearchBox = ({
  onInput,
  followUser,
  unFollowUser,
  searchResults,
  closeDropDown,
  isAlreadyFollowed,
}) => (
  <GridContainer justify="center" style={{ width: '97%' }}>
    <GridItem xs={11} style={{ margin: '10px 0px 0 0' }}>
      <SearchInput placeholder="Search..." onInput={onInput} />
      {!!searchResults.length && (
        <ClickAwayListener onClickAway={closeDropDown}>
          <FilteredUserList
            list={searchResults}
            followUser={followUser}
            unFollowUser={unFollowUser}
            isAlreadyFollowed={isAlreadyFollowed}
          />
        </ClickAwayListener>
      )}
    </GridItem>
  </GridContainer>
);

ProfileViewSearchBox.propTypes = {
  onInput: PropTypes.func.isRequired,
  followUser: PropTypes.func.isRequired,
  unFollowUser: PropTypes.func.isRequired,
  searchResults: PropTypes.array.isRequired,
  closeDropDown: PropTypes.func.isRequired,
  isAlreadyFollowed: PropTypes.func.isRequired,
};

export default ProfileViewSearchBox;
