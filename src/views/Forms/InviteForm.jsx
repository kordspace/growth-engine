import React from 'react';
import { Link } from 'react-router-dom';

// // core components
import CustomInput from 'components/CustomInput/CustomInput.jsx';
import Button from 'components/CustomButtons/Button.jsx';
import Card from "components/Card/Card.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";

const InviteForm = () => {
  return (
    <div>
      <Card
        style={{
          padding: '20px',
          backgroundColor: '#21384a',
        }}
      >
        <GridContainer justify="center">
          <GridItem xs={8}>
            <h2 style={{ color: '#ffffff' }}>
              <b>Invite a Friend</b>
            </h2>
            <h3
              style={{
                fontSize: '22px',
                color: '#ffffff',
              }}
            >
              <b>Earn $1 per invite. Maximum 5 invites per day.</b>
            </h3>
          </GridItem>
          <GridItem xs={3}>
            <Card style={{ padding: '20px' }}>
              <GridContainer justify="center">
                <GridItem xs={6}>
                  <h3 style={{ color: '#ffffff' }}>
                    <b>Remaining invites</b>
                  </h3>
                </GridItem>
                <GridItem xs={3}>
                  <div
                    style={{
                      backgroundColor: '#faeb60',
                      margin: '25px 0',
                      borderRadius: '50px',
                      height: '65px',
                      width: '65px',
                      padding: '15px 13px',
                    }}
                  >
                    <h3
                      style={{ color: '#21384a', margin: '0', textAlign: 'center' }}
                    >
                      <b>5</b>
                    </h3>
                  </div>
                </GridItem>
              </GridContainer>
            </Card>
          </GridItem>
        </GridContainer>
        <br />
        <GridContainer justify="center">
          <GridItem xs={8}>
            <CustomInput labelText="Email adress" id="email_adress" />
          </GridItem>
          <GridItem xs={3}>
            <Button
              style={{
                width: '100%',
                margin: '15px 0px 0px 0px',
                padding: '18px 20px',
                borderRadius: '10px',
                fontSize: '16px',
                backgroundColor: '#009874',
                textTransform: 'uppercase',
              }}
            >
              <b>Send Invite</b>
            </Button>
          </GridItem>
        </GridContainer>
      </Card>
      <GridContainer justify="center">
        <GridItem xs={12}>
          <Link
            to={{
              pathname: '/rewards-fees/',
            }}
          >
            {/* <GridContainer justify="center"> */}
            <GridItem xs={12}>
              <Button
                style={{
                  width: '100%',
                  margin: '15px 0px 0px 0px',
                  padding: '20px',
                  borderRadius: '10px',
                  fontSize: '16px',
                  backgroundColor: '#009874',
                  textTransform: 'uppercase',
                }}>
                <b>View Rewards List</b>
              </Button>
            </GridItem>
            {/* </GridContainer> */}
          </Link>
        </GridItem>
      </GridContainer>
    </div>
  );
};

export default InviteForm;
