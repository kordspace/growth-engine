import React from 'react';
import GridContainer from 'components/Grid/GridContainer.jsx';
import GridItem from 'components/Grid/GridItem.jsx';
import SearchInput from 'components/SearchInput';
import CustomDropdown from 'components/CustomDropdown/CustomDropdown.jsx';

export const CategorySearch = () => (
  <GridContainer style={{ marginTop: '20px' }}>
    <GridItem xs={9}>
      <SearchInput color="#629886" onInput={() => null} />
    </GridItem>
    <GridItem xs={3}>
      <CustomDropdown
        buttonText="Sort By"
        dropdownList={[
          'Recent',
          { divider: true },
          'Views',
          { divider: true },
          'Rating',
        ]}
      />
    </GridItem>
  </GridContainer>
);

export default CategorySearch;
