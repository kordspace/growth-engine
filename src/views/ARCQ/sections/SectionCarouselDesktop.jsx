import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import compose from "recompose/compose";

// react component for creating beautiful carousel
import Carousel from "react-slick";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// @material-ui/icons
import LocationOn from "@material-ui/icons/LocationOn";
import Loader from "assets/img/preloader.gif";

// core components
import carouselStyle from "assets/jss/material-dashboard-pro-react/views/componentsSections/carouselStyle.jsx";
import Button from "components/CustomButtons/Button.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Card from "components/Card/Card.jsx";
import { Grid } from "@material-ui/core";

// Firebase Redux Integration
import { startSetPendingActionsList } from "store/actions/actionsActions";
import {
  tableToFirestoreObjects,
  updateFirestoreWithTableData,
  addFieldToDocument
} from "store/actions/firestoreActions";
import { startSetUsersList } from "store/actions/adminActions";
import { startSetCasesList } from "store/actions/casesActions";

// Images
import trending1 from "assets/img/arcq/forest.png";
import trending3 from "assets/img/arcq/gameboy.png";
import trending5 from "assets/img/arcq/lightTunnel.png";
import trending2 from "assets/img/trending1.jpg";
import trending4 from "assets/img/trending2.jpg";
import trending6 from "assets/img/trending3.jpg";

class SectionCarouselDesktop extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dataLoaded: false,
      assignActionModal: false,
      volUID: "",
      username: "",
      query: ""
    };
  }

  loadData = () => {
    let usersData = this.props.users.dataRows.map((prop, key) => {
      let colObj = { ...prop };
      const volUID = colObj.volUID;
      const username = colObj.username;
      // Load Data for Summary Cards and add buttons
      return {
        ...colObj,
        // status: (<span style={{ color: "red" }}>assigned</span>),
        id: key,
        actions: (
          <Button
            color="primary"
            fullWidth
            onClick={() => this.handleClickOpen("assignActionModal", volUID)}
          >
            ASSIGN ACTION
          </Button>
        )
      };
    });

    this.setState({
      usersData,
      dataLoaded: true
    });
  };

  render() {
    const { AuthBool, classes, user } = this.props;

    const settings = {
      dots: true,
      infinite: true,
      speed: 1000,
      slidesToShow: 4,
      slidesToScroll: 1,
      autoplay: true
    };

    if (AuthBool) {
      if (this.props.users.dataLoaded && !this.state.dataLoaded) {
        this.loadData();
      }
    }

    const TrendingData = [
      {
        img: trending1,
        about: "My favorite things about nature are..."
      },
      {
        img: trending2,
        about: "Does life have any meaning?"
      },
      {
        img: trending3,
        about: "My favorite family tradition is..."
      },
      {
        img: trending4,
        about: "Should we fund ocean exploration?"
      },
      {
        img: trending5,
        about: "Is there Life after death?"
      },
      {
        img: trending6,
        about: "Which is better? Saving money, or enjoying life..."
      }
    ];

    return (
      <div>
        {this.state.dataLoaded ? (
          <div>
            <Carousel
              style={{ borderRadius: "15px", height: "200px" }}
              {...settings}
              id="home"
            >
              {TrendingData.map((data, index) => (
                <GridItem xs={12} key={index}>
                  <Link
                    to={{
                      pathname: `/view-post`
                    }}
                  >
                    <div>
                      <img
                        style={{
                          padding: "5px",
                          height: "200px",
                          borderRadius: "15px"
                        }}
                        src={data.img}
                        alt="Slide1"
                        className="slick-image"
                      />
                      <div
                        style={{ textAlign: "left" }}
                        className="slick-caption"
                      >
                        <h5 style={{ fontWeight: "600" }}>{data.about}</h5>
                      </div>
                    </div>
                  </Link>
                </GridItem>
              ))}
            </Carousel>
          </div>
        ) : (
  <div
    style={{
      background: '#000',
      position: 'absolute',
      width: '110%',
      marginLeft: '-7%',
      height: '100%',
    }}
  >
    {/* Desktop */}
      <Hidden xsDown implementation="css">
      <img
      style={{
        margin: '45% 0% 0% 41%',
        position: 'relative',
        width: '20%',
      }}
      src={LoaderGif}
      alt="..."
      />
      </Hidden>
      {/* Mobile */}
        <Hidden smUp implementation="css">
    <img
      style={{
        margin: '65% 0% 0% 41%',
        position: 'relative',
        width: '20%',
      }}
      src={LoaderGif}
      alt="..."
    />
    </Hidden>
  </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    AuthBool: !!state.auth.user,
    users: state.admin.users,
    user: state.auth.user,
    cases: state.cases.cases
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  console.log(this.props);
  return {
    startSetUsersList: dispatch(startSetUsersList())
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(carouselStyle)
)(SectionCarouselDesktop);
