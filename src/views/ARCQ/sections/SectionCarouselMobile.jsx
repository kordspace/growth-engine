import React from 'react';
import { Link } from 'react-router-dom';

// react component for creating beautiful carousel
import Carousel from 'react-slick';

// core components
import GridItem from 'components/Grid/GridItem.jsx';

// Images
import trending1 from 'assets/img/arcq/forest.png';
import trending3 from 'assets/img/arcq/gameboy.png';
import trending5 from 'assets/img/arcq/lightTunnel.png';
import trending2 from 'assets/img/trending1.jpg';
import trending4 from 'assets/img/trending2.jpg';
import trending6 from 'assets/img/trending3.jpg';

class SectionCarouselMobile extends React.Component {
  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 1000,
      slidesToShow: 2,
      slidesToScroll: 1,
      autoplay: true,
    };

    const TrendingData = [
      {
        img: trending1,
        about: 'My favorite things about nature are...',
      },
      {
        img: trending2,
        about: 'Does life have any meaning?',
      },
      {
        img: trending3,
        about: 'My favorite family tradition is...',
      },
      {
        img: trending4,
        about: 'Should we fund ocean exploration?',
      },
      {
        img: trending5,
        about: 'Is there Life after death?',
      },
      {
        img: trending6,
        about: 'Which is better? Saving money, or enjoying life...',
      },
    ];

    return (
      <div>
        <Carousel
          style={{ borderRadius: '15px', height: '200px' }}
          {...settings}
          id="home"
        >
          {TrendingData.map((data, index) => (
            <GridItem xs={12} key={index}>
              <Link
                to={{
                  pathname: '/view-post',
                }}
              >
                <div>
                  <img
                    style={{
                      padding: '5px',
                      height: '200px',
                      borderRadius: '15px',
                    }}
                    src={data.img}
                    alt="Slide1"
                    className="slick-image"
                  />
                  <div style={{ textAlign: 'left' }} className="slick-caption">
                    <h5 style={{ fontWeight: '600' }}>{data.about}</h5>
                  </div>
                </div>
              </Link>
            </GridItem>
          ))}
        </Carousel>
      </div>
    );
  }
}

export default SectionCarouselMobile;
