import React from "react";
import { connect } from "react-redux";
import compose from "recompose/compose";
import { db } from "firebase/fbConfig.js";
import ChartistGraph from "react-chartist";

// react component for creating dynamic tables
import ReactTable from "react-table";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Slide from "@material-ui/core/Slide";

// @material-ui/icons
import Assignment from "@material-ui/icons/Assignment";
import Dvr from "@material-ui/icons/Dvr";
import Favorite from "@material-ui/icons/Favorite";
import Check from "@material-ui/icons/Check";
import Close from "@material-ui/icons/Close";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import ArrowDownward from "@material-ui/icons/ArrowDownward";
import Timeline from "@material-ui/icons/Timeline";
import Loader from "assets/img/preloader.gif";

// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardIcon from "components/Card/CardIcon.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardFooter from "components/Card/CardFooter.jsx";

import Table from "components/Table/Table.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Search from "@material-ui/icons/Search";
import Accordion from "components/Accordion/Accordion.jsx";
import NavPills from "components/NavPills/NavPills.jsx";

import { cardTitle } from "assets/jss/material-dashboard-pro-react.jsx";
import Style from "assets/jss/material-dashboard-pro-react/views/chatStyle";
import { Grid } from "@material-ui/core";
import marc from "assets/img/faces/marc.jpg";
import avatar from "assets/img/faces/avatar.jpg";

import {
  roundedLineChart,
  straightLinesChart,
  simpleBarChart,
  colouredLineChart,
  multipleBarsChart,
  colouredLinesChart,
  pieChart
} from "variables/charts.jsx";

// Forms
import AssignActionForm from "components/Forms/AssignActionForm.jsx";

// Firebase Redux Integration
import { startSetPendingActionsList } from "store/actions/actionsActions";
import {
  tableToFirestoreObjects,
  updateFirestoreWithTableData,
  addFieldToDocument
} from "store/actions/firestoreActions";
import { startSetUsersList } from "store/actions/adminActions";

const Transition = props => {
  return <Slide direction="down" {...props} />;
};

class Users extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dataLoaded: false,
      assignActionModal: false,
      volUID: "",
      username: ""
    };
  }

  handleClickOpen = (modal, volUID) => {
    var x = [];
    x[modal] = true;
    this.setState({
      ...x,
      volUID
    });
  };

  handleClose = modal => {
    var x = [];
    x[modal] = false;
    this.setState(x);
  };

  loadData = () => {
    let usersData = this.props.users.dataRows.map((prop, key) => {
      let colObj = { ...prop };
      const volUID = colObj.volUID;
      const username = colObj.username;
      // Load Data for Summary Cards and add buttons
      return {
        ...colObj,
        // status: (<span style={{ color: "red" }}>assigned</span>),
        id: key,
        actions: (
          <Button
            color="primary"
            fullWidth
            onClick={() => this.handleClickOpen("assignActionModal", volUID)}
          >
            ASSIGN ACTION
          </Button>
        )
      };
    });

    this.setState({
      usersData,
      dataLoaded: true
    });
  };

  handleChange = docID => {
    console.log(docID);
  };

  render() {
    const { AuthBool, classes } = this.props;
    const searchButton = classes.top + " " + classes.searchButton;

    if (AuthBool) {
      if (this.props.users.dataLoaded && !this.state.dataLoaded) {
        this.loadData();
      }
    }

    return (
      <div>
        {this.state.dataLoaded ? (
          <div>
            <GridContainer>
              {/*<GridItem xs={12} sm={12} md={4}>
              <Card chart>
                <CardHeader color="rose">
                  <ChartistGraph
                    className="ct-chart-white-colors"
                    data={roundedLineChart.data}
                    type="Line"
                    options={roundedLineChart.options}
                    listener={roundedLineChart.animation}
                  />
                </CardHeader>
                <CardBody>
                  <h4 className={classes.cardTitle}>Rounded Line Chart</h4>
                  <p className={classes.cardCategory}>Line Chart</p>
                </CardBody>
              </Card>
            </GridItem>
            <GridItem xs={12} sm={12} md={4}>
              <Card chart>
                <CardHeader color="warning">
                  <ChartistGraph
                    className="ct-chart-white-colors"
                    data={straightLinesChart.data}
                    type="Line"
                    options={straightLinesChart.options}
                    listener={straightLinesChart.animation}
                  />
                </CardHeader>
                <CardBody>
                  <h4 className={classes.cardTitle}>Straight Lines Chart</h4>
                  <p className={classes.cardCategory}>Line Chart with Points</p>
                </CardBody>
              </Card>
    </GridItem>*/}
              <GridItem xs={12} sm={12} md={4}>
                <Card chart>
                  <CardHeader color="info">
                    <ChartistGraph
                      className="ct-chart-white-colors"
                      data={simpleBarChart.data}
                      type="Bar"
                      options={simpleBarChart.options}
                      responsiveOptions={simpleBarChart.responsiveOptions}
                      listener={simpleBarChart.animation}
                    />
                  </CardHeader>
                  <CardBody>
                    <h4 className={classes.cardTitle}>Ad Spend and Reach</h4>
                    <p className={classes.cardCategory}>Bar Chart</p>
                  </CardBody>
                </Card>
              </GridItem>
            </GridContainer>
            <GridContainer>
              <GridItem xs={4} sm={4} md={4}>
                <Card>
                  <CardHeader color="info" icon>
                    {/*<CardIcon color="info">
                    <Timeline />
    </CardIcon>*/}
                    <h3 className={classes.cardTitle}>
                      <b>105</b>
                    </h3>
                    <p style={{ color: "white" }}>
                      <small>Profile Hits</small>
                    </p>
                  </CardHeader>
                  {/* <CardBody>
                  <ChartistGraph
                    data={colouredLineChart.data}
                    type="Line"
                    options={colouredLineChart.options}
                    listener={colouredLineChart.animation}
                  />
                </CardBody>*/}
                </Card>
              </GridItem>
              <GridItem xs={4} sm={4} md={4}>
                <Card>
                  <CardHeader color="rose" icon>
                    {/*<CardIcon color="rose">
                    <Timeline />
    </CardIcon>*/}
                    <h3 className={classes.cardTitle}>
                      <b>$7.5k</b>
                    </h3>
                    <p style={{ color: "white" }}>
                      <small>Monthly Retainer Total</small>
                    </p>
                  </CardHeader>
                  {/* <CardBody>
                  <ChartistGraph
                    data={multipleBarsChart.data}
                    type="Bar"
                    options={multipleBarsChart.options}
                    listener={multipleBarsChart.animation}
                  />
                </CardBody>*/}
                </Card>
              </GridItem>
              <GridItem xs={4} sm={4} md={4}>
                <Card>
                  <CardHeader color="rose" icon>
                    {/*<CardIcon color="rose">
                  <Timeline />
                </CardIcon>*/}
                    <h3 className={classes.cardTitle}>
                      <b>$90k</b>
                    </h3>
                    <p style={{ color: "white" }}>
                      <small>Yearly Retainer Total</small>
                    </p>
                  </CardHeader>
                  {/*  <CardBody>
                <ChartistGraph
                  data={multipleBarsChart.data}
                  type="Bar"
                  options={multipleBarsChart.options}
                  listener={multipleBarsChart.animation}
                />
              </CardBody>*/}
                </Card>
              </GridItem>
            </GridContainer>
            <GridContainer>
              <GridItem xs={12} sm={12} md={5}>
                <Card>
                  <CardHeader color="warning" icon>
                    {/*<CardIcon color="warning">
                    <Timeline />
                  </CardIcon>*/}
                    <h4 className={classes.cardIconTitle}>
                      Individual Case Metrics
                    </h4>
                  </CardHeader>
                  <CardBody>
                    <ChartistGraph
                      data={colouredLinesChart.data}
                      type="Line"
                      options={colouredLinesChart.options}
                      listener={colouredLinesChart.animation}
                    />
                  </CardBody>
                </Card>
              </GridItem>
              <GridItem xs={12} sm={12} md={5}>
                <Card>
                  <CardHeader color="danger" icon>
                    <CardIcon color="danger">
                      <Timeline />
                    </CardIcon>
                    <h4 className={classes.cardIconTitle}>Pie Chart</h4>
                  </CardHeader>
                  <CardBody>
                    <ChartistGraph
                      data={pieChart.data}
                      type="Pie"
                      options={pieChart.options}
                    />
                  </CardBody>
                  <CardFooter stats className={classes.cardFooter}>
                    <h6 className={classes.legendTitle}>Legend</h6>
                    <i className={"fas fa-circle " + classes.info} /> Apple
                    {` `}
                    <i className={"fas fa-circle " + classes.warning} /> Samsung
                    {` `}
                    <i className={"fas fa-circle " + classes.danger} /> Windows
                    Phone
                    {` `}
                  </CardFooter>
                </Card>
              </GridItem>
            </GridContainer>
          </div>
        ) : (
  <div
    style={{
      background: '#000',
      position: 'absolute',
      width: '110%',
      marginLeft: '-7%',
      height: '100%',
    }}
  >
    {/* Desktop */}
      <Hidden xsDown implementation="css">
      <img
      style={{
        margin: '45% 0% 0% 41%',
        position: 'relative',
        width: '20%',
      }}
      src={LoaderGif}
      alt="..."
      />
      </Hidden>
      {/* Mobile */}
        <Hidden smUp implementation="css">
    <img
      style={{
        margin: '65% 0% 0% 41%',
        position: 'relative',
        width: '20%',
      }}
      src={LoaderGif}
      alt="..."
    />
    </Hidden>
  </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    AuthBool: !!state.auth.user,
    users: state.admin.users
  };
};

const mapDispatchToProps = dispatch => {
  return {
    startSetUsersList: dispatch(startSetUsersList())
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(Style)
)(Users);
